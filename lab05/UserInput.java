/**
 * CSE 002
 * Michael Dressman
 * 10/4/18
 */
import java.util.Scanner; //imports scanner
public class UserInput {
  public static void main (String []args) {
    
    Scanner myScanner = new Scanner (System.in); //new scanner
    //initializes variables
    int courseNumber;
    String departmentName;
    int classesPerWeek;
    double startTime;
    String instructorName;
    int numberOfStudents;
    boolean integer;
    
    System.out.print("Type in Course Number (xxx): "); //prompts course number
    //do while loop that only stores variable if its an int
    do {
      integer = myScanner.hasNextInt();
      //if statement that stores coursenumber if its an int
      if (integer == true) {
        courseNumber = myScanner.nextInt();
      }
      else {
        myScanner.next();
        System.out.println("You need to input an integer");
      }
    } while (integer == false);
    
    System.out.print("Type in Department Name: ");
    do {
      integer = myScanner.hasNext();
      if (integer == true) {
        departmentName = myScanner.next();
      }
      else {
        myScanner.next();
        System.out.println("You need to input a String");
      }
    } while (integer == false);
    //do while loop that only stores variable if its an int
    System.out.print("Type in number of meeting times per week (x): ");
    do {
      integer = myScanner.hasNextInt();
      if (integer == true) {
        classesPerWeek = myScanner.nextInt();
      }
      else {
        myScanner.next();
        System.out.println("You need to input an integer");
      }
    } while (integer == false);
    //do while loop that only stores variable if its an double
    System.out.print("Type in class start time (xx.xx): ");
    do {
      integer = myScanner.hasNextDouble();
      //if statement that stores starttime if its a double
      if (integer == true) {
        startTime = myScanner.nextDouble();
      }
      else {
        myScanner.next();
        System.out.println("You need to input a double");
      }
    } while (integer == false);
    
    System.out.print("Type in Intstructor's Name: ");
    do {
      integer = myScanner.hasNext();
      if (integer == true) {
        instructorName = myScanner.next();
      }
      else {
        myScanner.next();
        System.out.println("You need to input a string");
      }
    } while (integer == false);
    //do while loop that only stores variable if its an int
    System.out.print("Type in number of students (xxx): ");
    do {
      integer = myScanner.hasNextInt();
      if (integer == true) {
        numberOfStudents = myScanner.nextInt();
      }
      else {
        myScanner.next();
        System.out.println("You need to input an integer");
      }
    } while (integer == false);
  }
}