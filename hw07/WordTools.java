import java.util.Scanner;
public class WordTools {
  public static void main (String []args) {
    String text = sampleText();
    boolean menu = true;
    do {
      char option = printMenu();
      switch(option) {
        case 'c': 
        int number = getNumOfNonWSCharacters(text);
        System.out.println("Number of non-whitespace characters: "+number);
        break;
        case 'w':
        int words = getNumOfWords(text);
        System.out.println("Number of words: "+words);
        break;
        case 'f':
        Scanner foundScanner = new Scanner (System.in);
        System.out.println("Enter a word or phrase to be found");
        String sample = foundScanner.next();
        int instances = findText(text,sample);
        System.out.println("\"" +sample + "\" instances: " +instances);
        break; 
        case 'r':
        String newText = replaceExplanation(text);
        System.out.println("Edited text: "+newText);
        break;
        case 's':
        String shortenText = shortenSpace(text);
        System.out.println("Edited text: "+shortenText);
        break;
        case 'q': menu = false;
        break;
        default: System.out.println("Enter valid option");
        break;
        }
    } while (menu == true);
  }

  public static String sampleText() {
    Scanner textScanner = new Scanner (System.in);
    System.out.println("Enter a sample text: ");
    String text = textScanner.nextLine();
    System.out.println("You entered: "+text);
    return text;
  }

  public static char printMenu() {
    Scanner optionScanner = new Scanner (System.in);
    System.out.println("MENU");
    System.out.println("c - Number of non-whitespace characters");
    System.out.println("w - Number of words");
    System.out.println("f - Find text");
    System.out.println("r - Replace all !\'s");
    System.out.println("s - Shorten spaces");
    System.out.println("q - Quit");
    System.out.println();
    System.out.println("Choose an option:");
    char option = optionScanner.next().charAt(0);
    return option;
  }

  public static int getNumOfNonWSCharacters(String text) {
    int length = text.length();
    for (int x = 0; x <= (length-1); x++) {
      if (text.charAt(x) == ' ') {
        length--;
      }
    }
    return length;
  }

  public static int getNumOfWords(String text) {
    int words = 0;
    int length = text.length();
    for (int x = 0; x <= (length-1); x++) {
      if (text.charAt(x) == ' ' && text.charAt(x-1) != ' ') {
        words++;
      }
    }
      if (text.charAt(length-1) != ' ') {
        words++;
      }
    return words;
  }
  
  public static int findText(String text, String sample) {
    int instances = 0;
    String fixText = "";
    int length = text.length();
    for (int x = 0; x <= (length-1); x++) {
      fixText += text.charAt(x);
      if (fixText.contains(sample)) {
        instances++;
        fixText = "";
      }
    }
    return instances;
  }

  public static String replaceExplanation(String text) {
    int length = text.length();
    String replaceText = "";
    for (int x = 0; x <= (length-1); x++) {
      if (text.charAt(x) == '!') {
        replaceText += ".";
      }
      else {
        replaceText += text.charAt(x);
      }
    }
    return replaceText;
  }

  public static String shortenSpace(String text) {
    int length = text.length();
    String shortenText = "";
    for (int x = 0; x <= (length-1); x++) {
      if(text.charAt(x) == ' ' && text.charAt(x-1) == ' ') {
      //blank if statement
      }
      else {
        shortenText += text.charAt(x);
      }
    }
    return shortenText;
  }
}








