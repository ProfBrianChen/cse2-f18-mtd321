/*
Michael Dressman
9/11/18
CSE 002 HW 2
Arithmetic program that calculates subtotal and total costs using tax rates on different clothing items
*/
public class Arithmetic {
  public static void main(String args[]) {
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;

    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;

    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;

    //the tax rate
    double paSalesTax = 0.06;
    
    //declares total cost variables
    double totalCostPants;
    double totalCostShirts;
    double totalCostBelts;
    
    //calculates the total costs of each item
    totalCostPants = numPants * pantsPrice;
    totalCostShirts = numShirts * shirtPrice;
    totalCostBelts = numBelts * beltCost;
    
    //declares variables for the tax of each item
    double pantsTax;
    double shirtTax;
    double beltTax;
    
    //calculates the tax for each item
    pantsTax = totalCostPants * paSalesTax;
    shirtTax = totalCostShirts * paSalesTax;
    beltTax = totalCostBelts * paSalesTax;
    
    //declares subtotal and total tax
    double subtotal;
    double totalTax;
    
    //calculates subtotal and total tax
    subtotal = totalCostPants + totalCostShirts + totalCostBelts;
    totalTax = pantsTax + shirtTax + beltTax;
    
    //formats totalTax to two decimal places
    totalTax = totalTax * 100;
    int totalTaxTwoDecimals = (int)totalTax;
    totalTax = totalTaxTwoDecimals / 100.0;
    
    //declares the total cost of the purchases
    double totalCost;
    
    //calculates total cost of the purchases
    totalCost = subtotal + totalTax;
    
    //prints out the subtotal, totalTax and totalCost
    System.out.println("The subtotal of the purchase is: $" + subtotal);
    System.out.println("The total sales tax of the purchase is: $" + totalTax);
    System.out.println("The total cost of the purchase is: $" + totalCost);
    
  }
}