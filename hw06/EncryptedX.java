import java.util.Scanner; //imports scanner
public class EncryptedX {
	public static void main (String []args) {
    boolean correct;
    int integer = 0;
		Scanner myScanner = new Scanner (System.in); //creates new scanner
		System.out.print("Enter integer 0-100: "); //prompts reader
    do {
      correct = myScanner.hasNextInt();
      //if statement that stores integer if its an int
      if (correct == true) {
        integer = myScanner.nextInt();
      }
      else {
        myScanner.next();
        System.out.println("You need to input an integer");
      }
    } while(correct==false);
    
		for (int row = 1; row <= integer+1; row++) { //for loop that makes rows and runs from 1 to integer plus 1
			for (int column = 1; column <= integer+1; column++) { //nested for loop that makes columns
				if (column == row || column == (integer+2-row)) { //if statement that decides where a space goes
					System.out.print(" ");
				}
				else {
					System.out.print("*");
				}
			}
			System.out.println(); //returns line
		}
	}
}

