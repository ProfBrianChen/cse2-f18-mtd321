////////////
///CSE 002
////
public class WelcomeClass {
  public static void main(String args[]) {
    //prints out welcome signature
    System.out.println("   -----------");
    System.out.println("   | WELCOME |");
    System.out.println("   -----------");
    System.out.println("   ^  ^  ^  ^  ^  ^  ");
    System.out.println("  / \\/ \\/ \\/ \\/ \\/ \\"); //overides key for backslash by using two successive backslashes
    System.out.println(" <-M--T--D--1--2--3->"); //signature
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
  }
}