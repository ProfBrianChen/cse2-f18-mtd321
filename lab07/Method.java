import java.util.Random;
import java.util.Scanner;
public class Method {
    public static void main (String [] args) {
		String answer = "y"; 
		Scanner myScanner = new Scanner (System.in);
    Random randomGenerator = new Random();
		do {
			String adjective1 = adjective(randomGenerator.nextInt(10));
			String adjective2 = adjective(randomGenerator.nextInt(10));
			String subject = subject(randomGenerator.nextInt(10));
			String verb = verb(randomGenerator.nextInt(10));
			String object = subject(randomGenerator.nextInt(10));
      String sentence = sentence(adjective1,adjective2,subject,verb,object);
      System.out.println(sentence);
      String sentence2 = sentence2(subject);
      System.out.println(sentence2);
      String sentence3 = sentence3(subject);
      System.out.println(sentence3);
			System.out.println("Would you like to generate another paragraph? (y/n)");
			answer = myScanner.next();
    } while (answer.equals("y"));
  }
  
  public static String sentence(String adjective1, String adjective2, String subject, String verb, String object) {
    String sentence = "The " + adjective1 + " " + subject + " " + verb + " the " + adjective2 + " " + object;
      return sentence;
  }
  
  public static String sentence2(String subject) {
    Scanner myScanner = new Scanner (System.in);
    Random randomGenerator = new Random();
    String sentence2 = pronoun(randomGenerator.nextInt(4),subject) +" used "+noun(randomGenerator.nextInt(10)) +" to " +actionVerb(randomGenerator.nextInt(10))
                      +" the " +noun(randomGenerator.nextInt(10));
    return sentence2;
  }
  
  public static String sentence3(String subject) {
    Scanner myScanner = new Scanner (System.in);
    Random randomGenerator = new Random();
    String sentence3 = "The "+subject +" "+pastVerb(randomGenerator.nextInt(10)) +" "
      +pronoun2(randomGenerator.nextInt(3))+" "+subject(randomGenerator.nextInt(10));
    return sentence3;
  }
  
  public static String pastVerb(int num1) {
	  String result;
	  switch (num1) {
		  case 0: result = "knew";
		  break;
		  case 1: result = "remembered";
      break;
      case 2: result = "heard";
      break;
      case 3: result = "tried";
      break;
      case 4: result = "helped";
      break;
      case 5: result = "learned";
      break;
      case 6: result = "committed";
      break;
      case 7: result = "became";
      break;
      case 8: result = "embraced";
      break;
      case 9: result = "forgot";
      break;
      default: result = "ERROR";
      break;
		}
		return result;
  }
  
  public static String noun(int num1) {
	  String result;
	  switch (num1) {
		  case 0: result = "cars";
		  break;
		  case 1: result = "guns";
      break;
      case 2: result = "eyes";
      break;
      case 3: result = "hands";
      break;
      case 4: result = "feet";
      break;
      case 5: result = "teeth";
      break;
      case 6: result = "tissues";
      break;
      case 7: result = "cups";
      break;
      case 8: result = "braces";
      break;
      case 9: result = "blankets";
      break;
      default: result = "ERROR";
      break;
		}
		return result;
  }
  
  public static String actionVerb(int num1) {
	  String result;
	  switch (num1) {
		  case 0: result = "fight";
		  break;
		  case 1: result = "be";
      break;
      case 2: result = "hurt";
      break;
      case 3: result = "help";
      break;
      case 4: result = "touch";
      break;
      case 5: result = "pass";
      break;
      case 6: result = "spew";
      break;
      case 7: result = "attack";
      break;
      case 8: result = "defend";
      break;
      case 9: result = "annoy";
      break;
      default: result = "ERROR";
      break;
		}
		return result;
  }
  
  public static String pronoun(int num1, String subject) {
    String result;
    switch (num1) {
      case 0: result = "He";
        break;
      case 1: result = "She";
        break;
      case 2: result = "It";
        break;
      case 3: result = subject;
        break;
      default: result = "ERROR";
        break;
    }
    return result;
    }
  
  public static String pronoun2(int num1) {
    String result;
    switch (num1) {
      case 0: result = "her";
        break;
      case 1: result = "his";
        break;
      case 2: result = "it's";
        break;
      default: result = "ERROR";
        break;
    }
    return result;
    }
  
  public static String adjective(int num1) {
	  String result;
	  switch (num1) {
		  case 0: result = "hairy";
		  break;
		  case 1: result = "fast";
      break;
      case 2: result = "slow";
      break;
      case 3: result = "brave";
      break;
      case 4: result = "kind";
      break;
      case 5: result = "soft";
      break;
      case 6: result = "stern";
      break;
      case 7: result = "mean";
      break;
      case 8: result = "hungry";
      break;
      case 9: result = "fluffy";
      break;
      default: result = "ERROR";
      break;
		}
		return result;
  }
  
  public static String subject(int num1) {
	  String result;
	  switch (num1) {
      case 0: result = "dog";
      break;
      case 1: result = "cat";
      break;
      case 2: result = "turtle";
      break;
      case 3: result = "snake";
      break;
      case 4: result = "elephant";
      break;
      case 5: result = "mouse";
      break;
      case 6: result = "anteater";
      break;
      case 7: result = "tiger";
      break;
      case 8: result = "lion";
      break;
      case 9: result = "bear";
      break;
      default: result = "ERROR";
      break;
		}
		return result;
  }
  
  public static String verb(int num1) {
	  String result;
	  switch (num1) {
      case 0: result = "passed";
      break;
      case 1: result = "ate";
      break;
      case 2: result = "punched";
      break;
      case 3: result = "kissed";
      break;
      case 4: result = "befriended";
      break;
      case 5: result = "attracted";
      break;
      case 6: result = "scared";
      break;
      case 7: result = "spooked";
      break;
      case 8: result = "tricked";
      break;
      case 9: result = "angered";
      break;
      default: result = "ERROR";
      break;
		}
		return result;
  }
  
}