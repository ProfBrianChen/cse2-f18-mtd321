/**
 * CSE 002
 * Michael Dressman
 * 10/11/18
 */
 import java.util.Scanner;
 public class PatternA {
   public static void main (String [] args) {
     Scanner myScanner = new Scanner (System.in);
     System.out.print("Enter a number 1-10: ");
     int integer = myScanner.nextInt();
     for (int row = 1; row <= integer; row++) {
       for (int column = 1; column <= row; column++) {
         System.out.print(column + " ");
       }
       System.out.println();
     }
   }
 }