/**
 * CSE 002
 * Michael Dressman
 * 10/11/18
 */
 import java.util.Scanner;
 public class PatternC {
   public static void main (String [] args) {
     Scanner myScanner = new Scanner (System.in);
     System.out.print("Enter a number 1-10: ");
     int integer = myScanner.nextInt();
     for (int row = 1; row <= integer; row++) {
       for (int space = 1; space <= (integer-row); space++) {
         System.out.print(" ");
       }
       for (int column = row; column > 0; column--) {
         System.out.print(column);
       }
       System.out.println();
     }
   }
 }