/* 
Michael Dressman
CSE 002 HW 03
9/18/18
This program prompts user to input sidelength and height to calculate the volume of a pyramid
*/
import java.util.Scanner; //imports scanner

public class Pyramid {
  
  public static void main (String[] args) {
    
    Scanner myScanner = new Scanner( System.in ); //creates new scanner
    
    System.out.print("The square side of the pyramid is (input length): "); //prompts user to input side length
    
    double sideLength = myScanner.nextDouble(); //scans for double
    
    System.out.print("The height of the pyramid is (input height): ");
    
    double height = myScanner.nextInt();
    
    double volume = sideLength * sideLength * height * (1.0/3.0); //calcuates the volume of a pyramid using the formula (1/3) *sideLength^2 * height
    
    System.out.println("The volume inside the pyramid is: " + volume); //prints output value
    
  }
  
}