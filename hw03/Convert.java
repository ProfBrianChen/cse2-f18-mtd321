/* 
Michael Dressman
CSE 002 HW 03
9/18/18
This program prompts user for area in acres and rainfail in inches and converts these values to cubic milesof rain
*/
import java.util.Scanner; //imports scanner

public class Convert {
  
  public static void main (String[] args) {
    
    Scanner myScanner = new Scanner( System.in ); //creates new scanner
    
    System.out.print("Enter the affected area in acres in the form xx.xx: "); //prompts reader for area in acres
    
    double area = myScanner.nextDouble(); //scans for double
    
    System.out.print("Enter the average rainfall in the area in the form xx: ");
    
    int rainfall = myScanner.nextInt(); //scans for int
    
    //declares and assigns constant variables for conversion
    
    final double ACRESPERMILE = 640.00;
    
    final double INCHESPERMILE = 63360.00;
    
    double cubicMiles = (area/ACRESPERMILE)*(rainfall/INCHESPERMILE); //converts to cubic miles
    
    System.out.println("There are " + cubicMiles + " cubic miles of rainfall");
    
  }
  
}