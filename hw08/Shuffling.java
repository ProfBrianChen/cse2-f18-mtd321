import java.util.Scanner;
import java.util.Random;
public class Shuffling{ 
public static void main(String[] args) { 
Scanner scan = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
 String[] suitNames={"C","H","S","D"};    
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
String[] cards = new String[52]; 
String[] hand = new String[5]; 
int numCards = 5; 
int again = 1; 
int index = 51;
for (int i=0; i<52; i++){ 
cards[i]=rankNames[i%13]+suitNames[i/13]; 
  	System.out.print(cards[i]+" "); 
} 
System.out.println();
printArray(cards); 
shuffle(cards); 
printArray(cards); 
while(again == 1){ 
hand = getHand(cards,index,numCards); 
printArray(hand);
index = index - numCards;
System.out.println("Enter a 1 if you want another hand drawn"); 
again = scan.nextInt(); 
}  
} 


public static void printArray(String [] list) {
	System.out.print("Array:");
	for (int x = 0; x < list.length; x++) {
		System.out.print(" "+list[x]);
	}
  System.out.println();
}



public static String[] shuffle(String [] list) {
	Random randomGenerator = new Random();
  String [] holdList = new String [list.length];
	for (int x = 0; x < list.length; x++) {
		int y = randomGenerator.nextInt(list.length-1)+1;
		holdList[0] = list[0];
		list[0] = list[y];
		list[y] = holdList[0];
	}
	return list;
}

public static String [] getHand(String [] list,int index,int numCards) {
	String [] array = new String [numCards];
	int count = 0;
	if (numCards > index+1) {
		list = shuffle(list);
}
	for (int x = (index-numCards+1); x <= index; x++) {
		array[count] = list[x];
		count++;
}
  count = 0;
return array;
}

}
