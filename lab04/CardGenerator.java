/*
CSE 002 Lab 04
Michael Dressman
9/20/18
This program uses Math.random() to generate random numbers which correspond to a suit and another to a card to generate a card from a deck
*/
public class CardGenerator {
  public static void main (String [] args) {
    
    int randInt = (int)((Math.random()*52)+1); //generates random number between 1-52
    
    //declares the Strings for the suit and the card
    String suit;
    String card;
    
    
    //if, else if, and else statements that determine the suit
    if (randInt >= 1 && randInt <= 13) {
      suit = "Clubs";
    }
    else if (randInt >= 14 && randInt <= 26) {
      suit = "Spades";
      randInt -= 13; //subtracts 13 to use randInt to assign a card
    }
    else if (randInt >= 27 && randInt <= 39) {
      suit = "Hearts";
      randInt -= 26;
    }
    else if (randInt >= 40 && randInt <= 52) {
      suit = "Diamonds";
      randInt -= 39;
    }
    else {
      suit = "Invalid";
    }
    
    //if, else if, and else statements that determine the card based off randInt
    if (randInt == 1) {
      card = "Ace";
    }
    else if (randInt == 2) {
      card = "2";
    }
    else if (randInt == 3) {
      card = "3";
    }
    else if (randInt == 4) {
      card = "4";
    }
    else if (randInt == 5) {
      card = "5";
    }
    else if (randInt == 6) {
      card = "6";
    }
    else if (randInt == 7) {
      card = "7";
    }
    else if (randInt == 8) {
      card = "8";
    }
    else if (randInt == 9) {
      card = "9";
    }
    else if (randInt == 10) {
      card = "10";
    }
    else if (randInt == 11) {
      card = "Jack";
    }
    else if (randInt == 12) {
      card = "Queen";
    }
    else if (randInt == 13) {
      card = "King";
    }
    else {
      card = "Invalid";
    }
    
    System.out.println("You picked the " + card + " of " + suit); //print statement
  }
}