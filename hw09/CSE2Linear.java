import java.util.Scanner; //imports clases
import java.util.Random;
public class CSE2Linear {
 public static void main (String []args) { //main method
  boolean grade;
  Scanner myScanner = new Scanner (System.in);
  int grades [] = new int[15]; //creates array
System.out.print("Enter 15 scending ints for final grades in CSE2: ");
for (int x = 0; x < 15; x++) { //for loop that runs 15 times
  do { //do while loop that runs if one condition isnt met
   grade = myScanner.hasNextInt();
   if (grade == true) { //if grade is an int it adss to array
grades[x] = myScanner.nextInt();
if (grades[x] >= 100 || grades[x] <= 0) { //if statement for error
System.out.println("Enter integer from 0-100");
grade = false;
}
if (x>0) {
if (grades[x] <= grades[x-1]) { //if statement for error
 System.out.println("Enter an integer greater than the last integer");
grade = false;
}
}
     }
     else {
        myScanner.next();
        System.out.println("Enter an integer");
}
  } while(grade == false);
  }
  for (int y = 0; y < 15; y++) { //prints array
   System.out.print(grades[y] + " ");
  }
System.out.println();
System.out.print("Enter a grade to search for: ");
int search = myScanner.nextInt();
binarySearch(grades,search);
scramble(grades);

}
 public static void binarySearch (int [] grades, int search) { //binary search
   int low = 0;
   int iterations = 0;
   int high = grades.length - 1;
   boolean bool = false;
   while (high >= low && bool == false) {
     iterations++;
     int mid = (low+high)/2;
     if (search < grades[mid]) {
       high = mid - 1;
     }
     else if (search == grades[mid]) {
       System.out.println(search +" was found with "+iterations + " iterations"); //prints out iteratios used to find match
       bool = true;
     }
     else {
       low = mid +1;
     }
     if (low > high) {
       System.out.println(search +" was not found with "+iterations + " iterations");
     }
   }
 }
 
 public static void scramble (int [] grades) {
   Random randomGen = new Random();
   int scramble [] = new int [15];
   Scanner myScanner = new Scanner (System.in);
   for (int x = 0; x < 15; x++) {
     scramble[x] = grades[randomGen.nextInt(15)]; //randomely generates number for array component
   }
   System.out.println("Scrambled: ");
   for (int y = 0; y < 15; y++) {
   System.out.print(scramble[y] + " ");
  }
   System.out.print("Enter a grade to search for: ");
   int search = myScanner.nextInt();
   linear(scramble, search); // calls linear search
   }
 
 public static void linear (int [] scramble, int search) {
   int test = 0;
   for (int i = 0; i < scramble.length; i++) {
     if (search == scramble[i]) { //checks each array component
       System.out.println(search +" was found with "+(i+1) + " iterations");
       test++;
     }
   }
   if (test == 0) {
     System.out.println(search +" was not found with 15 iterations");
   }
 }
}


