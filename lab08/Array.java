import java.util.Random; //imports random class
public class Array {
	public static void main (String []args) {
		Random randomGenerator = new Random(); //initializes randomGenerator
		int [] array1 = new int [100]; //initializes the first array 
		for (int x = 0; x < 100; x++) { //for loop that puts random ints in the array
			array1[x] = randomGenerator.nextInt(100);
		}
		int [] array2 = new int [100]; //intitializes the second array
    for (int y = 0; y < 100; y++) { //for loop that fills array2 with 0 
			array2[y] = 0;
		}
		for (int z = 0; z < 100; z++) { //adds 1 to each array location in array 2 for each number in array1
				array2[array1[z]] += 1;	
		}
		System.out.print("Array 1 holds the following integers:");
		for (int q = 0; q < 100; q++) { //prints array1
			System.out.print(" " +array1[q]);
		}
		System.out.println();
		System.out.println();
		for (int r = 0; r < 100; r++) { //prints array2 occurances
			System.out.println(r + " occurs " +array2[r] +" times");
		}
	}
}
