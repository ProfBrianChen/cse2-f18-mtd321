// CSE 002
// Michael Dressman
// 9/6/18
// Cyclometer program that documents trip time and distance
public class Cyclometer {
  // main method
  public static void main(String[] args) {
    // variables declared and assigned
    int secsTrip1=480;  //time of trip 1 in sec
    int secsTrip2=3220;  //time pf trip 2 in sec
	  int countsTrip1=1561;  //tire rotations in trip 1
    int countsTrip2=9037; //tire rotations in trip 2
    double wheelDiameter=27.0;  //diameter of wheel
    double PI=3.14159; //value of PI
    int feetPerMile=5280;  //conversion of feet in a mile
    int inchesPerFoot=12;   //conversion of inches in a foot
 	  int secondsPerMinute=60;  //conversion of seconds in a minute
    double distanceTrip1;
    double distanceTrip2;
    double totalDistance;  //declares variables to be assigned values later
    System.out.println("Trip 1 took "+((double)secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts.");
    System.out.println("Trip 2 took "+((double)secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");
    distanceTrip1=countsTrip1*wheelDiameter*PI;
   	// Above gives distance in inches
   	//(for each count, a rotation of the wheel travels
   	//the diameter in inches times PI)
    distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    totalDistance=distanceTrip1+distanceTrip2; 
    //Print out the output data.
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");
	}  //end of main method   
} //end of class
