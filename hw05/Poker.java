/**
 * CSE 002
 * Michael Dressman
 * 10/9/18
 */
import java.util.Scanner;//imports scanner
public class Poker {
    public static void main (String [] args) {
        Scanner pokerScanner = new Scanner (System.in); //new scanner
        System.out.print("Enter number of hands to be generated: ");//prompt
        int hands = pokerScanner.nextInt();//reads number of hands to be drawn
      //initiales and assigns zero to counters
        int loopCounter = 1, fourKindCounter = 0, threeKindCounter = 0, twoKindCounter = 0, twoPairCounter = 0;
      ////intitializes and assigns zeros to each card
        int card1 = 0, card2 = 0, card3 = 0, card4 = 0, card5 = 0;
      //intitializes and assigns zeros to each card number
        int ace = 0, two= 0, three = 0, four = 0, five = 0, six = 0, seven = 0, eight = 0, nine = 0, ten = 0, jack = 0, queen = 0, king = 0;
        int error = 0;
      //while loop to find number of times a hand is found
        while (loopCounter < hands) {
          //random number 1-52 assigned to card1
            card1 = (int)((Math.random()*52)+1);
          //random number 1-52 assigned to card2
            card2 = (int)((Math.random()*52)+1);
          //while loop to change card2 to a unique card
          while (card2 == card3) {
                card2 = (int)((Math.random()*52)+1);
            }
            card3 = (int)((Math.random()*52)+1);
            while (card3 == card2 || card3 == card1) {
                card3 = (int)((Math.random()*52)+1);
            }
            card4 = (int)((Math.random()*52)+1);
            while (card4 == card3 || card4 == card2 || card4 == card1) {
                card4 = (int)((Math.random()*52)+1);
            }
            card5 = (int)((Math.random()*52)+1);
            while (card5 == card4 || card5 == card3 || card5 == card2 || card4 == card1) {
                card5 = (int)((Math.random()*52)+1);
            }
          //switch statement to sort card 1 by card
            switch(card1) {
                case 1:  case 14: case 27: case 40: ace++;
                break;
                case 2: case 15: case 28: case 41: two++;
                break;
                case 3: case 16: case 29: case 42: three++;
                break;
                case 4: case 17: case 30: case 43: four++;
                break;
                case 5: case 18: case 31: case 44: five++;
                break;
                case 6: case 19: case 32: case 45: six++;
                break;
                case 7: case 20: case 33: case 46: seven++;
                break;
                case 8: case 21: case 34: case 47: eight++;
                break;
                case 9: case 22: case 35: case 48: nine++;
                break;
                case 10: case 23: case 36: case 49: ten++;
                break;
                case 11: case 24: case 37: case 50: jack++;
                break;
                case 12: case 25: case 38: case 51: queen++;
                break;
                case 13: case 26: case 39: case 52: king++;
                break;
                default: error++;
                break;
            }
          //switch statement to sort card 2 by card
            switch(card2) {
                case 1:  case 14: case 27: case 40: ace++;
                break;
                case 2: case 15: case 28: case 41: two++;
                break;
                case 3: case 16: case 29: case 42: three++;
                break;
                case 4: case 17: case 30: case 43: four++;
                break;
                case 5: case 18: case 31: case 44: five++;
                break;
                case 6: case 19: case 32: case 45: six++;
                break;
                case 7: case 20: case 33: case 46: seven++;
                break;
                case 8: case 21: case 34: case 47: eight++;
                break;
                case 9: case 22: case 35: case 48: nine++;
                break;
                case 10: case 23: case 36: case 49: ten++;
                break;
                case 11: case 24: case 37: case 50: jack++;
                break;
                case 12: case 25: case 38: case 51: queen++;
                break;
                case 13: case 26: case 39: case 52: king++;
                break;
                default: error++;
                break;
            }
          //switch statement to sort card 3 by card
            switch(card3) {
                case 1:  case 14: case 27: case 40: ace++;
                break;
                case 2: case 15: case 28: case 41: two++;
                break;
                case 3: case 16: case 29: case 42: three++;
                break;
                case 4: case 17: case 30: case 43: four++;
                break;
                case 5: case 18: case 31: case 44: five++;
                break;
                case 6: case 19: case 32: case 45: six++;
                break;
                case 7: case 20: case 33: case 46: seven++;
                break;
                case 8: case 21: case 34: case 47: eight++;
                break;
                case 9: case 22: case 35: case 48: nine++;
                break;
                case 10: case 23: case 36: case 49: ten++;
                break;
                case 11: case 24: case 37: case 50: jack++;
                break;
                case 12: case 25: case 38: case 51: queen++;
                break;
                case 13: case 26: case 39: case 52: king++;
                break;
                default: error++;
                break;
            }
          //switch statement to sort card 4 by card
            switch(card4) {
                case 1:  case 14: case 27: case 40: ace++;
                break;
                case 2: case 15: case 28: case 41: two++;
                break;
                case 3: case 16: case 29: case 42: three++;
                break;
                case 4: case 17: case 30: case 43: four++;
                break;
                case 5: case 18: case 31: case 44: five++;
                break;
                case 6: case 19: case 32: case 45: six++;
                break;
                case 7: case 20: case 33: case 46: seven++;
                break;
                case 8: case 21: case 34: case 47: eight++;
                break;
                case 9: case 22: case 35: case 48: nine++;
                break;
                case 10: case 23: case 36: case 49: ten++;
                break;
                case 11: case 24: case 37: case 50: jack++;
                break;
                case 12: case 25: case 38: case 51: queen++;
                break;
                case 13: case 26: case 39: case 52: king++;
                break;
                default: error++;
                break;
            }
          //switch statement to sort card 5 int by card
            switch(card5) {
                case 1:  case 14: case 27: case 40: ace++;
                break;
                case 2: case 15: case 28: case 41: two++;
                break;
                case 3: case 16: case 29: case 42: three++;
                break;
                case 4: case 17: case 30: case 43: four++;
                break;
                case 5: case 18: case 31: case 44: five++;
                break;
                case 6: case 19: case 32: case 45: six++;
                break;
                case 7: case 20: case 33: case 46: seven++;
                break;
                case 8: case 21: case 34: case 47: eight++;
                break;
                case 9: case 22: case 35: case 48: nine++;
                break;
                case 10: case 23: case 36: case 49: ten++;
                break;
                case 11: case 24: case 37: case 50: jack++;
                break;
                case 12: case 25: case 38: case 51: queen++;
                break;
                case 13: case 26: case 39: case 52: king++;
                break;
                default: error++;
                break;
            }
            
            //if statement used to find four of a kind
            if (ace == 4 || two == 4 || three == 4 || four == 4 || five == 4 || six == 4 || seven == 4 || 
                eight == 4 || nine == 4 || ten == 4 || jack == 4 || queen == 4 || king == 4) {
                    fourKindCounter++;
            }
          //else if statement used to find three of a kind
            else if (ace == 3 || two == 3 || three == 3 || four == 3 || five == 3 || six == 3 || seven == 3 || 
                eight == 3 || nine == 3 || ten == 3 || jack == 3 || queen == 3 || king == 3) {
                    threeKindCounter++;
            }
          //else if statement used to find two pairs
            else if ((ace == 2 && (two == 2 || three == 2 || four == 2 || five == 2 || six == 2 || seven == 2 || eight == 2 || nine == 2 
                || ten ==  2 || jack == 2 || queen == 2 || king == 2)) || (two == 2 && (three == 2 || four == 2 || five == 2 || six == 2 
                || seven == 2 || eight == 2 || nine == 2 || ten ==  2 || jack == 2 || queen == 2 || king == 2)) 
                || (three == 2 && (four == 2 || five == 2 || six == 2 || seven == 2 || eight == 2 || nine == 2 || ten ==  2 || jack == 2 
                || queen == 2 || king == 2)) || (four == 2 && (five == 2 || six == 2 || seven == 2 || eight == 2 || nine == 2 || ten ==  2 
                || jack == 2 || queen == 2 || king == 2)) || (five == 2 && (six == 2 || seven == 2 || eight == 2 || nine == 2 || ten ==  2 
                || jack == 2 || queen == 2 || king == 2)) || (six == 2 && (seven == 2 || eight == 2 || nine == 2 || ten ==  2 || jack == 2 
                || queen == 2 || king == 2)) || (seven == 2 && (eight == 2 || nine == 2 || ten ==  2 || jack == 2 || queen == 2 || king == 2)) 
                || (eight == 2 && (nine == 2 || ten ==  2 || jack == 2 || queen == 2 || king == 2)) || (nine == 2 && (ten ==  2 || jack == 2 
                || queen == 2 || king == 2)) || (ten == 2 && (jack == 2 || queen == 2 || king == 2)) || (jack == 2 && (queen == 2 
                || king == 2)) || (queen == 2 && king == 2)) { 
                    twoPairCounter++;
            }
          //else if statement used to find one pair
            else if (ace == 2 || two == 2 || three == 2 || four == 2 || five == 2 || six == 2 || seven == 2 || 
                eight == 2 || nine == 2 || ten == 2 || jack == 2 || queen == 2 || king == 2) {
                    twoKindCounter++;
            }
            loopCounter++; //adds one to loop counter
            ace = 0;
          //resets all counts to 0
            two= 0; 
            three = 0; 
            four = 0; 
            five = 0;
            six = 0; 
            seven = 0; 
            eight = 0; 
            nine = 0; 
            ten = 0;
            jack = 0;
            queen = 0; 
            king = 0;
        }
      //calculating the percent chance of each hand
        double percentFourKind = ((double)(fourKindCounter)/(double)hands);
        double percentThreeKind = ((double)(threeKindCounter)/(double)hands);
        double percentTwoPair = ((double)(twoPairCounter)/(double)hands);
        double percentTwoKind = ((double)(twoKindCounter)/(double)hands);
      //printlines that format the percents to 3 decimal places
        System.out.println("The number of loops: "+hands);
        System.out.printf("The probability of Four-of-a-kind: %2.3f\n",percentFourKind);
        System.out.printf("The probability of Three-of-a-kind: %2.3f\n",percentThreeKind);
        System.out.printf("The probability of Two-pair: %2.3f\n",percentTwoPair);
        System.out.printf("The probability of One-pair: %2.3f\n",percentTwoKind);
    }
}

