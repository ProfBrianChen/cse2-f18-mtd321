import java.util.Scanner;
public class test {
	public static void main (String []args) {
		Scanner myScanner = new Scanner (System.in);
		System.out.println("Enter integer 0-100: ");
		int integer = myScanner.nextInt();
		for (int row = 1; row <= integer+1; row++) {
			for (int column = 1; column <= integer+1; column++) {
				if (column == row || column == (integer+2-row)) {
					System.out.print(" ");
				}
				else {
					System.out.print("*");
				}
			}
			System.out.println();
		}
	}
}
