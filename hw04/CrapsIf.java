/* 
CSE 002 HW 4
Michael Dressman
9/23/18
This program
*/
import java.util.Scanner; //imports scanner
public class CrapsIf {
  public static void main (String[] args) {
    
    Scanner crapsScanner = new Scanner( System.in ); //creates new craps scanner
    
    //initializes variables
    int dieOne;
    int dieTwo;
    String slang;
    
    System.out.print("Enter the number 1 to roll the two die or enter number 2 to choose the two die: "); //prompts user to either roll die or choose die
    int choice = crapsScanner.nextInt(); //reads int
    
    if (choice == 1) { //if statement for rolling die
      dieOne = (int)((Math.random()*6)+1); //uses math.random to generate die sides 1-6
      dieTwo = (int)((Math.random()*6)+1);
    }
    else if (choice == 2) { //if statements for choosing die
      System.out.println("Enter a number 1-6 for die one: "); //prompts reader for die side
      dieOne = crapsScanner.nextInt(); //reads int
      System.out.println("Enter a number 1-6 for die two: ");
      dieTwo = crapsScanner.nextInt();
    }
    else { //else statement to grab anything other than 1 or 2
      dieOne = 8;
      dieTwo = 8;
      System.out.println("Choice is invalid"); //prints if invalid option
    }
    
    //if statements that determine slang based on die combinations
    
    if (dieOne == 1 && dieTwo == 1) {
      slang = "Snake Eyes";
    }
    else if (dieOne == 1 && dieTwo == 2 || dieOne == 2 && dieTwo == 1) { //includes different orders of die rolls
      slang = "Ace Deuce";
    }
    else if (dieOne == 1 && dieTwo == 3 || dieOne == 3 && dieTwo == 1) {
      slang = "Easy Four";
    }
    else if (dieOne == 1 && dieTwo == 4 || dieOne == 4 && dieTwo == 1 || dieOne == 2 && dieTwo == 3 || dieOne == 3 && dieTwo == 2) {
      slang = "Fever Five";
    }
    else if (dieOne == 1 && dieTwo == 5 || dieOne == 5 && dieTwo == 1 || dieOne == 2 && dieTwo == 4 || dieOne == 4 && dieTwo == 2) {
      slang = "Easy Six";
    }
    else if (dieOne == 1 && dieTwo == 6 || dieOne == 6 && dieTwo == 1 || dieOne == 2 && dieTwo == 5 || dieOne == 5 && dieTwo == 2 || dieOne == 3 && dieTwo == 4 || dieOne == 4 && dieTwo == 3) {
      slang = "Seven Out";
    }
    else if (dieOne == 2 && dieTwo == 2) {
      slang = "Hard Four";
    }
    else if (dieOne == 2 && dieTwo == 6 || dieOne == 6 && dieTwo == 2 || dieOne == 3 && dieTwo == 5 || dieOne == 5 && dieTwo == 3) {
      slang = "Easy Eight";
    }
    else if (dieOne == 3 && dieTwo == 3) {
      slang = "Hard Six";
    }
    else if (dieOne == 3 && dieTwo == 6 || dieOne == 6 && dieTwo == 3 || dieOne == 4 && dieTwo == 5 || dieOne == 5 && dieTwo == 4) {
      slang = "Nine";
    }
    else if (dieOne == 4 && dieTwo == 4) {
      slang = "Hard Eight";
    }
    else if (dieOne == 4 && dieTwo == 6 || dieOne == 6 && dieTwo == 4) {
      slang = "Easy Ten";
    }
    else if (dieOne == 5 && dieTwo == 5) {
      slang = "Hard Ten";
    }
    else if (dieOne == 5 && dieTwo == 6 || dieOne == 6 && dieTwo == 5) {
      slang = "Yo-leven";
    }
    else if (dieOne == 6 && dieTwo == 6) {
      slang = "Boxcars";
    }
    else { //else statement to grab any other combos and prints invalid
      slang = "Invalid";
      System.out.println("Invalid die");
    }
    
    System.out.println("You rolled/chose: " + slang); //prints slang for die roll
  }
}