/* 
CSE 002 HW 4
Michael Dressman
9/23/18
This program uses switch statements to determine the slang word for random/choosen die roll in craps
*/
import java.util.Scanner; //imports scanner
public class CrapsSwitch {
  public static void main (String[] args) {
    
    Scanner crapsScanner = new Scanner( System.in ); //creates scanner for craps
    
    //initializes variables
    
    int dieOne;
    int dieTwo;
    String slang;
    
    System.out.print("Enter the number 1 to roll the two die or enter number 2 to choose the two die: "); //prompts reader to either roll/choose die
    int choice = crapsScanner.nextInt(); //reads int
    
    //switch statement that reads choice and chooses to roll or choose die
    switch (choice) {
      case 1: dieOne = (int)((Math.random()*6)+1); //case 1 if choice =1, randomly chooses die side 1-6
      dieTwo = (int)((Math.random()*6)+1);
        break;
      case 2: System.out.println("Enter a number 1-6 for die one: "); //case 2 that prompts reader for die sides
      dieOne = crapsScanner.nextInt();
      System.out.println("Enter a number 1-6 for die two: ");
      dieTwo = crapsScanner.nextInt();
        break;
      default: dieOne = 8; //default that grabs anything other than int = 1 or 2
        dieTwo = 8;
        break;
    }
    
    switch (dieOne) { //switch statement that reads dieOne and has case 1-6 for each side
      case 1: 
        switch(dieTwo) { //nestled switch statement for dieTwo for side 1-6 to determine slang word
          case 1: slang = "Snake Eyes";
            break;
          case 2: slang = "Ace Deuce";
            break;
          case 3: slang = "Easy Four";
            break;
          case 4: slang = "Fever Five";
            break;
          case 5: slang = "Easy Six";
            break;
          case 6: slang = "Seven Out";
            break;
          default: slang = "Invalid";
            break;
        }
        break;
      case 2:
        switch(dieTwo) { //nestled switch statement for dieTwo for side 1-6 to determine slang word
          case 1: slang = "Ace Deuce";
            break;
          case 2: slang = "Hard Four";
            break;
          case 3: slang = "Fever Five";
            break;
          case 4: slang = "Easy Six";
            break;
          case 5: slang = "Seven Out";
            break;
          case 6: slang = "Easy Eight";
            break;
          default: slang = "Invalid";
            break;
        }
        break;
        case 3:
        switch(dieTwo) { //nestled switch statement for dieTwo for side 1-6 to determine slang word
          case 1: slang = "Easy Four";
            break;
          case 2: slang = "Fever Five";
            break;
          case 3: slang = "Hard Six";
            break;
          case 4: slang = "Seven Out";
            break;
          case 5: slang = "Easy Eight";
            break;
          case 6: slang = "Nine";
            break;
          default: slang = "Invalid";
            break;
        }
        break;
        case 4:
        switch(dieTwo) { //nestled switch statement for dieTwo for side 1-6 to determine slang word
          case 1: slang = "Fever Five";
            break;
          case 2: slang = "Easy Six";
            break;
          case 3: slang = "Seven Out";
            break;
          case 4: slang = "Hard Eight";
            break;
          case 5: slang = "Nine";
            break;
          case 6: slang = "Easy Ten";
            break;
          default: slang = "Invalid";
            break;
        }
        break;
        case 5:
        switch(dieTwo) { //nestled switch statement for dieTwo for side 1-6 to determine slang word
          case 1: slang = "Easy Six";
            break;
          case 2: slang = "Seven Out";
            break;
          case 3: slang = "Easy Eight";
            break;
          case 4: slang = "Nine";
            break;
          case 5: slang = "Hard Ten";
            break;
          case 6: slang = "Yo-leven";
            break;
          default: slang = "Invalid";
            break;
        }
        break;
        case 6:
        switch(dieTwo) { //nestled switch statement for dieTwo for side 1-6 to determine slang word
          case 1: slang = "Seven Out";
            break;
          case 2: slang = "Easy Eight";
            break;
          case 3: slang = "Nine";
            break;
          case 4: slang = "Easy Ten";
            break;
          case 5: slang = "Yo-leven";
            break;
          case 6: slang = "Boxcars";
            break;
          default: slang = "Invalid";
            break;
        }
        break;
      default: slang = "Invalid";
        break;
    }
    
    System.out.println("You rolled/chose: " + slang); //prints slang term for craps
  }
}